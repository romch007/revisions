import {
  Entity,
  BaseEntity,
  PrimaryGeneratedColumn,
  Column,
  OneToOne
} from 'typeorm';
import { ObjectType, Field, ID } from 'type-graphql';

import { Admin, Student, Teacher } from './';

@Entity()
@ObjectType()
export class User extends BaseEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: string;

  @Field()
  @Column()
  username: string;

  @Column()
  password: string;

  @Field({ nullable: true })
  @Column({ nullable: true })
  email?: string;

  @Field(() => Student, { nullable: true })
  @OneToOne(() => Student, student => student.user, { lazy: true })
  student?: Promise<Student>;

  @Field(() => Teacher, { nullable: true })
  @OneToOne(() => Teacher, teacher => teacher.user, { lazy: true })
  teacher?: Promise<Teacher>;

  @Field(() => Admin, { nullable: true })
  @OneToOne(() => Admin, admin => admin.user, { lazy: true })
  admin?: Promise<Admin>;
}
