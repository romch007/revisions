import {
  Entity,
  BaseEntity,
  PrimaryGeneratedColumn,
  Column,
  OneToOne,
  JoinColumn,
  ManyToMany
} from 'typeorm';
import { ObjectType, Field, ID } from 'type-graphql';

import { User, Group, Lesson } from './';
import { Lazy } from '../helpers';

@Entity()
@ObjectType()
export class Student extends BaseEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: string;

  @Field()
  @Column()
  firstname: string;

  @Field()
  @Column()
  lastname: string;

  @Field()
  get displayName(): string {
    return `${this.firstname} ${this.lastname}`;
  }

  @Field()
  @Column({ default: false })
  absent: boolean;

  @Field(() => User)
  @OneToOne(() => User, { lazy: true, onDelete: 'CASCADE' })
  @JoinColumn()
  user: Lazy<User>;

  // ManyToMany relations

  @Field(() => [Group])
  @ManyToMany(() => Group, group => group.students, {
    lazy: true,
    onDelete: 'CASCADE'
  })
  groups: Lazy<Group[]>;

  @Field(() => [Lesson])
  @ManyToMany(() => Lesson, lesson => lesson.students, {
    lazy: true,
    onDelete: 'CASCADE'
  })
  lessons: Lazy<Lesson[]>;
}
