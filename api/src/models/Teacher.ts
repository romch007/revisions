import {
  Entity,
  BaseEntity,
  PrimaryGeneratedColumn,
  Column,
  OneToOne,
  JoinColumn,
  ManyToMany,
  JoinTable
} from 'typeorm';
import { ObjectType, Field, ID } from 'type-graphql';

import { User, Lesson, Subject } from './';
import { Lazy } from '../helpers';

@Entity()
@ObjectType()
export class Teacher extends BaseEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: string;

  @Field()
  @Column()
  firstname: string;

  @Field()
  @Column()
  lastname: string;

  @Field()
  get displayName(): string {
    return `${this.firstname} ${this.lastname}`;
  }

  @Field(() => User)
  @OneToOne(() => User, { lazy: true, onDelete: 'CASCADE' })
  @JoinColumn()
  user: Lazy<User>;

  // Many to many

  @Field(() => [Lesson])
  @ManyToMany(() => Lesson, lesson => lesson.teachers, {
    lazy: true,
    onDelete: 'CASCADE'
  })
  @JoinTable()
  lessons: Lazy<Lesson[]>;

  @Field(() => [Subject])
  @ManyToMany(() => Subject, subject => subject.teachers, {
    lazy: true,
    onDelete: 'CASCADE'
  })
  subjects: Lazy<Subject[]>;
}
