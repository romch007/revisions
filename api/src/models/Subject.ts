import {
  Entity,
  BaseEntity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  JoinTable
} from 'typeorm';
import { ObjectType, Field, ID } from 'type-graphql';

import { Teacher, Lesson } from './';
import { Lazy } from '../helpers';

@Entity()
@ObjectType()
export class Subject extends BaseEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: string;

  @Field()
  @Column()
  title: string;

  @Field({ nullable: true })
  @Column({ nullable: true })
  description?: string;

  // ManyToMany relations

  @Field(() => [Teacher])
  @ManyToMany(() => Teacher, teacher => teacher.subjects, {
    lazy: true,
    onDelete: 'CASCADE'
  })
  @JoinTable()
  teachers: Lazy<Teacher[]>;

  @Field(() => [Lesson])
  @ManyToMany(() => Lesson, lesson => lesson.subjects, {
    lazy: true,
    onDelete: 'CASCADE'
  })
  @JoinTable()
  lessons: Lazy<Lesson[]>;
}
