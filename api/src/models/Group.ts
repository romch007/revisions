import {
  Entity,
  BaseEntity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  JoinTable
} from 'typeorm';
import { ObjectType, Field, ID } from 'type-graphql';

import { Student, Lesson } from './';
import { Lazy } from '../helpers';

@Entity()
@ObjectType()
export class Group extends BaseEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: string;

  @Field()
  @Column()
  title: string;

  @Field({ nullable: true })
  @Column({ nullable: true })
  description?: string;

  // ManyToMany relations

  @Field(() => [Student])
  @ManyToMany(() => Student, student => student.groups, {
    lazy: true,
    onDelete: 'CASCADE'
  })
  @JoinTable()
  students: Lazy<Student[]>;

  @Field(() => [Lesson])
  @ManyToMany(() => Lesson, lesson => lesson.groups, {
    lazy: true,
    onDelete: 'CASCADE'
  })
  @JoinTable()
  lessons: Lazy<Lesson[]>;
}
