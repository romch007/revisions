import {
  Entity,
  BaseEntity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany
} from 'typeorm';
import { ObjectType, Field, ID } from 'type-graphql';

import { Lesson } from './';
import { Lazy } from '../helpers';

@Entity()
@ObjectType()
export class Classroom extends BaseEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: string;

  @Field()
  @Column()
  title: string;

  @Field(() => [Lesson])
  @OneToMany(() => Lesson, lesson => lesson.classroom, {
    lazy: true,
    onDelete: 'CASCADE'
  })
  lessons: Lazy<Lesson[]>;
}
