import {
  Entity,
  BaseEntity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  JoinTable,
  ManyToOne
} from 'typeorm';
import { ObjectType, Field, ID } from 'type-graphql';

import { Student, Group, Teacher, Subject, Classroom } from './';
import { Lazy } from '../helpers';

@Entity()
@ObjectType()
export class Lesson extends BaseEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: string;

  @Field()
  @Column()
  title: string;

  @Field({ nullable: true })
  @Column({ nullable: true })
  description?: string;

  @Field()
  @Column()
  startDate: Date;

  @Field()
  @Column()
  endDate: Date;

  // ManyToMany relations

  @Field(() => [Student])
  @ManyToMany(() => Student, student => student.lessons, {
    lazy: true,
    onDelete: 'CASCADE'
  })
  @JoinTable()
  students: Lazy<Student[]>;

  @Field(() => [Group])
  @ManyToMany(() => Group, group => group.lessons, {
    lazy: true,
    onDelete: 'CASCADE'
  })
  groups: Lazy<Group[]>;

  @Field(() => [Teacher])
  @ManyToMany(() => Teacher, teacher => teacher.lessons, {
    lazy: true,
    onDelete: 'CASCADE'
  })
  teachers: Lazy<Teacher[]>;

  @Field(() => [Subject])
  @ManyToMany(() => Subject, subject => subject.lessons, {
    lazy: true,
    onDelete: 'CASCADE'
  })
  subjects: Lazy<Subject[]>;

  @Field(() => Classroom, { nullable: true })
  @ManyToOne(() => Classroom, classroom => classroom.lessons, {
    lazy: true,
    onDelete: 'CASCADE'
  })
  classroom?: Lazy<Classroom>;
}
