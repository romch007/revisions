import {
  Entity,
  BaseEntity,
  PrimaryGeneratedColumn,
  Column,
  OneToOne,
  JoinColumn
} from 'typeorm';
import { ObjectType, Field, ID } from 'type-graphql';

import { User } from './';
import { Lazy } from '../helpers';

@Entity()
@ObjectType()
export class Admin extends BaseEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: string;

  @Field({ nullable: true })
  @Column({ nullable: true })
  firstname?: string;

  @Field({ nullable: true })
  @Column({ nullable: true })
  lastname?: string;

  @Field(() => User)
  @OneToOne(() => User, { lazy: true, onDelete: 'CASCADE' })
  @JoinColumn()
  user: Lazy<User>;
}
