import { InputType, Field } from 'type-graphql';

@InputType()
export class SubjectCreationInput {
  @Field()
  title: string;

  @Field({ nullable: true })
  description?: string;
}

@InputType()
export class SubjectModificationInput {
  @Field({ nullable: true })
  title?: string;

  @Field({ nullable: true })
  description?: string;
}
