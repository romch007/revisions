import { InputType, Field } from 'type-graphql';

@InputType()
export class ClassroomCreationInput {
  @Field()
  title: string;
}
