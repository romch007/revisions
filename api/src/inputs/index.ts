export * from './StudentInput';
export * from './TeacherInput';
export * from './SubjectInput';
export * from './AdminInput';
export * from './LessonInput';
export * from './ClassroomInput';
export * from './GroupInput';
