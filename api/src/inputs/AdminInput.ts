import { InputType, Field } from 'type-graphql';

@InputType()
export class AdminCreationInput {
  @Field({ nullable: true })
  firstname?: string;

  @Field({ nullable: true })
  lastname?: string;

  @Field()
  username: string;

  @Field({ nullable: true })
  email?: string;

  @Field()
  password: string;
}

@InputType()
export class AdminModificationInput {
  @Field({ nullable: true })
  firstname?: string;

  @Field({ nullable: true })
  lastname?: string;

  @Field({ nullable: true })
  username?: string;

  @Field({ nullable: true })
  email?: string;
}
