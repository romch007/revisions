import { InputType, ObjectType, Field, ID } from 'type-graphql';

@InputType()
export class StudentCreationInput {
  @Field()
  firstname: string;

  @Field()
  lastname: string;

  @Field()
  username: string;

  @Field({ nullable: true })
  email?: string;

  @Field()
  password: string;
}

@InputType()
export class StudentModificationInput {
  @Field({ nullable: true })
  firstname?: string;

  @Field({ nullable: true })
  lastname?: string;

  @Field({ nullable: true })
  username?: string;

  @Field({ nullable: true })
  email?: string;
}

@ObjectType()
export class StudentStateNotification {
  @Field()
  absent: boolean;

  @Field(() => ID)
  studentId: string;

  @Field()
  date: Date;
}

export class StudentStateNotificationPayload {
  absent: boolean;
  studentId: string;
}
