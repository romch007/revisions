import { InputType, Field } from 'type-graphql';

@InputType()
export class LessonCreationInput {
  @Field()
  title: string;

  @Field({ nullable: true })
  description?: string;

  @Field()
  startDate: Date;

  @Field()
  endDate: Date;
}

@InputType()
export class LessonModificationInput {
  @Field({ nullable: true })
  title?: string;

  @Field({ nullable: true })
  description?: string;

  @Field({ nullable: true })
  startDate?: Date;

  @Field({ nullable: true })
  endDate?: Date;
}
