import { InputType, Field } from 'type-graphql';

@InputType()
export class GroupCreationInput {
  @Field()
  title: string;

  @Field({ nullable: true })
  description?: string;
}

@InputType()
export class GroupModificationInput {
  @Field({ nullable: true })
  title?: string;

  @Field({ nullable: true })
  description?: string;
}
