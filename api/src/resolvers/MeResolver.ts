import {
  Resolver,
  Query,
  Arg,
  Mutation,
  Ctx,
  Authorized,
  ObjectType,
  Field
} from 'type-graphql';
import bcrypt from 'bcryptjs';

import { User, Admin, Student, Teacher } from '../models';
import { Context } from '../context';
import { JWTService, SignerType } from '../services';

@ObjectType()
export class LoginResult {
  @Field()
  token: string;

  @Field()
  role: string;

  @Field()
  profileId: string;

  @Field()
  userId: string;
}

@Resolver()
export class MeResolver {
  @Mutation(() => LoginResult)
  async login(
    @Arg('password') password: string,
    @Arg('username') username: string
  ): Promise<LoginResult | undefined> {
    const user = await User.findOneOrFail({ where: [{ username }] });
    const passwordMatches = await bcrypt.compare(password, user.password);
    if (passwordMatches) {
      let role: SignerType;
      let profileId: string;
      const student = await user.student;
      const teacher = await user.teacher;
      const admin = await user.admin;
      if (student) {
        role = 'STUDENT';
        profileId = student.id;
      } else if (teacher) {
        role = 'TEACHER';
        profileId = teacher.id;
      } else if (admin) {
        role = 'ADMIN';
        profileId = admin.id;
      } else {
        throw new Error('User do not have type');
      }
      const jwt = new JWTService();
      const token = await jwt.signToken(user.id, role);
      const loginResult = new LoginResult();
      loginResult.token = token;
      loginResult.role = role;
      loginResult.userId = user.id;
      loginResult.profileId = profileId;
      return loginResult;
    } else {
      throw new Error('Password do not matches');
    }
  }

  @Authorized('STUDENT')
  @Query(() => Student)
  async meStudent(@Ctx() { req }: Context) {
    const user = await User.findOne({
      where: [{ id: req.userInfo.userId }]
    });
    return user?.student;
  }
}
