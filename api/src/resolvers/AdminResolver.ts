import {
  Resolver,
  Query,
  Arg,
  ID,
  Mutation,
  FieldResolver,
  Root
} from 'type-graphql';
import bcrypt from 'bcryptjs';

import config from '../config.json';
import { User, Admin } from '../models';
import { AdminCreationInput, AdminModificationInput } from '../inputs';

@Resolver(() => Admin)
export class AdminResolver {
  @FieldResolver(() => String)
  async displayName(@Root() admin: Admin): Promise<string> {
    const isFirstnameDefined = !!admin.firstname;
    const isLastnameDefined = !!admin.lastname;
    if (isFirstnameDefined && isLastnameDefined)
      return `${admin.firstname} ${admin.lastname}`;
    else return (await admin.user).username;
  }

  @Query(() => [Admin])
  admins(): Promise<Admin[]> {
    return Admin.find();
  }

  @Query(() => Admin)
  admin(@Arg('id', () => ID) id: string): Promise<Admin> {
    return Admin.findOneOrFail({
      where: [{ id }]
    });
  }

  @Mutation(() => Admin)
  async createAdmin(
    @Arg('admin') inputAdmin: AdminCreationInput
  ): Promise<Admin> {
    const user = new User();
    user.username = inputAdmin.username;
    user.email = inputAdmin.email;
    user.password = await bcrypt.hash(
      inputAdmin.password,
      config.bcryptSaltRound
    );
    await user.save();

    const admin = new Admin();
    admin.user = user;
    admin.firstname = inputAdmin.firstname;
    admin.lastname = inputAdmin.lastname;
    await admin.save();

    return admin;
  }

  @Mutation(() => Admin)
  async modifyAdmin(
    @Arg('adminId', () => ID) adminId: string,
    @Arg('admin') inputAdmin: AdminModificationInput
  ): Promise<Admin> {
    const admin = await Admin.findOneOrFail({ where: [{ id: adminId }] });
    if (inputAdmin.firstname) admin.firstname = inputAdmin.firstname;
    if (inputAdmin.lastname) admin.lastname = inputAdmin.lastname;
    if (inputAdmin.username) (await admin.user).username = inputAdmin.username;
    if (inputAdmin.email) (await admin.user).email = inputAdmin.email;
    await (await admin.user).save();
    await admin.save();
    return admin;
  }

  @Mutation(() => Boolean)
  async deleteAdmin(
    @Arg('adminId', () => ID) adminId: string
  ): Promise<boolean> {
    const admin = await Admin.findOneOrFail({ where: [{ id: adminId }] });
    await Admin.remove(admin);
    return true;
  }
}
