import {
  Resolver,
  Query,
  Arg,
  ID,
  Mutation,
  Subscription,
  PubSub,
  Publisher,
  Root
} from 'type-graphql';
import bcrypt from 'bcryptjs';

import config from '../config.json';
import { User, Student } from '../models';
import {
  StudentCreationInput,
  StudentModificationInput,
  StudentStateNotification,
  StudentStateNotificationPayload
} from '../inputs';

@Resolver(() => Student)
export class StudentResolver {
  @Query(() => [Student])
  students(): Promise<Student[]> {
    return Student.find();
  }

  @Query(() => Student)
  student(@Arg('id', () => ID) id: string): Promise<Student> {
    return Student.findOneOrFail({
      where: [{ id }]
    });
  }

  @Mutation(() => Student)
  async createStudent(
    @Arg('student') inputStudent: StudentCreationInput
  ): Promise<Student> {
    const user = new User();
    user.username = inputStudent.username;
    user.email = inputStudent.email;
    user.password = await bcrypt.hash(
      inputStudent.password,
      config.bcryptSaltRound
    );
    await user.save();

    const student = new Student();
    student.user = user;
    student.firstname = inputStudent.firstname;
    student.lastname = inputStudent.lastname;
    await student.save();

    return student;
  }

  @Mutation(() => Student)
  async modifyStudent(
    @Arg('studentId', () => ID) studentId: string,
    @Arg('student') inputStudent: StudentModificationInput
  ): Promise<Student> {
    const student = await Student.findOneOrFail({ where: [{ id: studentId }] });
    if (inputStudent.firstname) student.firstname = inputStudent.firstname;
    if (inputStudent.lastname) student.lastname = inputStudent.lastname;

    if (inputStudent.username)
      (await student.user).username = inputStudent.username;
    if (inputStudent.email) (await student.user).email = inputStudent.email;
    await (await student.user).save();
    await student.save();
    return student;
  }

  @Mutation(() => Boolean)
  async deleteStudent(
    @Arg('studentId', () => ID) studentId: string
  ): Promise<boolean> {
    const student = await Student.findOneOrFail({ where: [{ id: studentId }] });
    await Student.remove(student);
    return true;
  }

  @Mutation(() => Boolean)
  async switchStudentState(
    @Arg('studentId', () => ID) studentId: string,
    @PubSub('STUDENT_STATE')
    publish: Publisher<StudentStateNotificationPayload>
  ): Promise<boolean> {
    const student = await Student.findOneOrFail({ where: [{ id: studentId }] });
    student.absent = !student.absent;
    await student.save();

    const payload: StudentStateNotificationPayload = {
      absent: student.absent,
      studentId: student.id
    };
    await publish(payload);

    return true;
  }

  @Subscription(() => StudentStateNotification, { topics: 'STUDENT_STATE' })
  studentStateModification(
    @Root() payload: StudentStateNotificationPayload
  ): StudentStateNotification {
    return { ...payload, date: new Date() };
  }
}
