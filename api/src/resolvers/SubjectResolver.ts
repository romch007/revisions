import { Resolver, Query, Arg, ID, Mutation } from 'type-graphql';

import { Subject } from '../models';
import { SubjectCreationInput, SubjectModificationInput } from '../inputs';

@Resolver(() => Subject)
export class SubjectResolver {
  @Query(() => [Subject])
  subjects(): Promise<Subject[]> {
    return Subject.find();
  }

  @Query(() => Subject)
  subject(@Arg('id', () => ID) id: string): Promise<Subject> {
    return Subject.findOneOrFail({
      where: [{ id }]
    });
  }

  @Mutation(() => Subject)
  async createSubject(
    @Arg('subject') subjectInput: SubjectCreationInput
  ): Promise<Subject> {
    const subject = new Subject();
    subject.title = subjectInput.title;
    subject.description = subjectInput.description;
    await subject.save();
    return subject;
  }

  @Mutation(() => Subject)
  async modifySubject(
    @Arg('subjectId', () => ID) subjectId: string,
    @Arg('subject') inputSubject: SubjectModificationInput
  ): Promise<Subject> {
    const subject = await Subject.findOneOrFail({ where: [{ id: subjectId }] });
    if (inputSubject.title) subject.title = inputSubject.title;
    if (inputSubject.description)
      subject.description = inputSubject.description;
    await subject.save();
    return subject;
  }

  @Mutation(() => Boolean)
  async deleteSubject(
    @Arg('subjectId', () => ID) subjectId: string
  ): Promise<boolean> {
    const subject = await Subject.findOneOrFail({ where: [{ id: subjectId }] });
    await Subject.remove(subject);
    return true;
  }
}
