import { Resolver, Query, Arg, ID, Mutation } from 'type-graphql';
import bcrypt from 'bcryptjs';

import config from '../config.json';
import { User, Teacher, Subject } from '../models';
import { TeacherCreationInput, TeacherModificationInput } from '../inputs';

@Resolver(() => Teacher)
export class TeacherResolver {
  @Query(() => [Teacher])
  teachers(): Promise<Teacher[]> {
    return Teacher.find();
  }

  @Query(() => Teacher)
  teacher(@Arg('id', () => ID) id: string): Promise<Teacher> {
    return Teacher.findOneOrFail({
      where: [{ id }]
    });
  }

  @Mutation(() => Teacher)
  async createTeacher(
    @Arg('teacher') inputTeacher: TeacherCreationInput
  ): Promise<Teacher> {
    const user = new User();
    user.username = inputTeacher.username;
    user.email = inputTeacher.email;
    user.password = await bcrypt.hash(
      inputTeacher.password,
      config.bcryptSaltRound
    );
    await user.save();

    const teacher = new Teacher();
    teacher.user = user;
    teacher.firstname = inputTeacher.firstname;
    teacher.lastname = inputTeacher.lastname;
    await teacher.save();

    return teacher;
  }

  @Mutation(() => Teacher)
  async modifyTeacher(
    @Arg('teacherId', () => ID) teacherId: string,
    @Arg('teacher') inputTeacher: TeacherModificationInput
  ): Promise<Teacher> {
    const teacher = await Teacher.findOneOrFail({ where: [{ id: teacherId }] });
    if (inputTeacher.firstname) teacher.firstname = inputTeacher.firstname;
    if (inputTeacher.lastname) teacher.lastname = inputTeacher.lastname;
    if (inputTeacher.username)
      (await teacher.user).username = inputTeacher.username;
    if (inputTeacher.email) (await teacher.user).email = inputTeacher.email;
    await (await teacher.user).save();
    await teacher.save();
    return teacher;
  }

  @Mutation(() => Boolean)
  async deleteTeacher(
    @Arg('teacherId', () => ID) teacherId: string
  ): Promise<boolean> {
    const teacher = await Teacher.findOneOrFail({ where: [{ id: teacherId }] });
    await Teacher.remove(teacher);
    return true;
  }

  @Mutation(() => Boolean)
  async assignSubjectsToTeacher(
    @Arg('teacherId', () => ID) teacherId: string,
    @Arg('subjectIds', () => [ID]) subjectIds: string[]
  ): Promise<boolean> {
    const teacher = await Teacher.findOneOrFail({ where: [{ id: teacherId }] });
    const subjects = await Subject.find({
      where: subjectIds.map((subjectId: string) => ({ id: subjectId }))
    });
    if (subjects.length == 0) return false;
    (await teacher.subjects).push(...subjects);
    await teacher.save();
    return true;
  }
}
