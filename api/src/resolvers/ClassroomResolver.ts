import { Resolver, Query, Arg, ID, Mutation } from 'type-graphql';

import { Classroom } from '../models';
import { ClassroomCreationInput } from '../inputs';

@Resolver(() => Classroom)
export class ClassroomResolver {
  @Query(() => [Classroom])
  classrooms(): Promise<Classroom[]> {
    return Classroom.find();
  }

  @Query(() => Classroom)
  classroom(@Arg('id', () => ID) id: string): Promise<Classroom> {
    return Classroom.findOneOrFail({ where: [{ id }] });
  }

  @Mutation(() => Classroom)
  async createClassroom(
    @Arg('classroom') inputClassroom: ClassroomCreationInput
  ): Promise<Classroom> {
    const classroom = new Classroom();
    classroom.title = inputClassroom.title;
    await classroom.save();
    return classroom;
  }

  @Mutation(() => Boolean)
  async deleteClassroom(
    @Arg('classroomId', () => ID) classroomId: string
  ): Promise<boolean> {
    const classroom = await Classroom.findOneOrFail({
      where: [{ id: classroomId }]
    });
    await Classroom.remove(classroom);
    return true;
  }
}
