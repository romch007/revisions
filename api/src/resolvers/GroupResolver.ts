import { Resolver, Query, Arg, ID, Mutation } from 'type-graphql';

import { Group, Student } from '../models';
import { GroupCreationInput, GroupModificationInput } from '../inputs';

@Resolver(() => Group)
export class GroupResolver {
  @Query(() => [Group])
  groups(): Promise<Group[]> {
    return Group.find();
  }

  @Query(() => Group)
  group(@Arg('id', () => ID) id: string): Promise<Group> {
    return Group.findOneOrFail({ where: [{ id }] });
  }

  @Mutation(() => Group)
  async createGroup(
    @Arg('group') inputGroup: GroupCreationInput
  ): Promise<Group> {
    const group = new Group();
    group.title = inputGroup.title;
    group.description = inputGroup.description;
    await group.save();

    return group;
  }

  @Mutation(() => Group)
  async modifyGroup(
    @Arg('groupId', () => ID) groupId: string,
    @Arg('group') inputGroup: GroupModificationInput
  ): Promise<Group> {
    const group = await Group.findOneOrFail({ where: [{ id: groupId }] });
    if (inputGroup.title) group.title = inputGroup.title;
    if (inputGroup.description) group.description = inputGroup.description;
    await group.save();
    return group;
  }

  @Mutation(() => Boolean)
  async deleteGroup(
    @Arg('groupId', () => ID) groupId: string
  ): Promise<boolean> {
    const group = await Group.findOneOrFail({ where: [{ id: groupId }] });
    await Group.remove(group);
    return true;
  }

  @Mutation(() => Boolean)
  async assignStudentsToGroup(
    @Arg('groupId', () => ID) groupId: string,
    @Arg('studentIds', () => [ID]) studentIds: string[]
  ): Promise<boolean> {
    const group = await Group.findOneOrFail({ where: [{ id: groupId }] });
    const students = await Student.find({
      where: studentIds.map((studentId: string) => ({ id: studentId }))
    });
    if (students.length == 0) return false;
    (await group.students).push(...students);
    await group.save();
    return true;
  }
}
