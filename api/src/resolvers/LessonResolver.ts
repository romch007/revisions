import { Resolver, Query, Arg, ID, Mutation } from 'type-graphql';

import { Lesson, Classroom, Student, Teacher, Group, Subject } from '../models';
import { LessonCreationInput, LessonModificationInput } from '../inputs';

@Resolver(() => Lesson)
export class LessonResolver {
  @Query(() => [Lesson])
  lessons(): Promise<Lesson[]> {
    return Lesson.find();
  }

  @Query(() => Lesson)
  lesson(@Arg('id', () => ID) id: string): Promise<Lesson> {
    return Lesson.findOneOrFail({ where: [{ id }] });
  }

  @Mutation(() => Lesson)
  async createLesson(
    @Arg('lesson') inputLesson: LessonCreationInput
  ): Promise<Lesson> {
    const lesson = new Lesson();
    lesson.title = inputLesson.title;
    lesson.description = inputLesson.description;
    lesson.startDate = inputLesson.startDate;
    lesson.endDate = inputLesson.endDate;
    await lesson.save();
    return lesson;
  }

  @Mutation(() => Lesson)
  async modifyLesson(
    @Arg('lessonId', () => ID) lessonId: string,
    @Arg('lesson') inputLesson: LessonModificationInput
  ): Promise<Lesson> {
    const lesson = await Lesson.findOneOrFail({ where: [{ id: lessonId }] });
    if (inputLesson.title) lesson.title = inputLesson.title;
    if (inputLesson.description) lesson.description = inputLesson.description;
    if (inputLesson.startDate) lesson.startDate = inputLesson.startDate;
    if (inputLesson.endDate) lesson.endDate = inputLesson.endDate;
    await lesson.save();
    return lesson;
  }

  @Mutation(() => Boolean)
  async deleteLesson(
    @Arg('lessonId', () => ID) lessonId: string
  ): Promise<boolean> {
    const lesson = await Lesson.findOneOrFail({ where: [{ id: lessonId }] });
    await Lesson.remove(lesson);
    return true;
  }

  @Mutation(() => Boolean)
  async assignClassroomToLesson(
    @Arg('classroomId', () => ID) classroomId: string,
    @Arg('lessonId', () => ID) lessonId: string
  ): Promise<boolean> {
    const classroom = await Classroom.findOneOrFail({
      where: [{ id: classroomId }]
    });
    const lesson = await Lesson.findOneOrFail({ where: [{ id: lessonId }] });
    lesson.classroom = classroom;
    await lesson.save();
    return true;
  }

  @Mutation(() => Boolean)
  async assignStudentsToLesson(
    @Arg('studentIds', () => [ID]) studentIds: string[],
    @Arg('lessonId', () => ID) lessonId: string
  ): Promise<boolean> {
    const students = await Student.find({
      where: studentIds.map((studentId: string) => ({ id: studentId }))
    });
    const lesson = await Lesson.findOneOrFail({ where: [{ id: lessonId }] });
    if (students.length == 0) return false;
    (await lesson.students).push(...students);
    await lesson.save();
    return true;
  }

  @Mutation(() => Boolean)
  async assignTeachersToLesson(
    @Arg('teacherIds', () => [ID]) teacherIds: string[],
    @Arg('lessonId', () => ID) lessonId: string
  ): Promise<boolean> {
    const teachers = await Teacher.find({
      where: teacherIds.map((teacherId: string) => ({ id: teacherId }))
    });
    const lesson = await Lesson.findOneOrFail({ where: [{ id: lessonId }] });
    if (teachers.length == 0) return false;
    (await lesson.teachers).push(...teachers);
    await lesson.save();
    return true;
  }

  @Mutation(() => Boolean)
  async assignGroupsToLesson(
    @Arg('groupIds', () => [ID]) groupIds: string[],
    @Arg('lessonId', () => ID) lessonId: string
  ): Promise<boolean> {
    const groups = await Group.find({
      where: groupIds.map((groupId: string) => ({ id: groupId }))
    });
    const lesson = await Lesson.findOneOrFail({ where: [{ id: lessonId }] });
    if (groups.length == 0) return false;
    (await lesson.groups).push(...groups);
    await lesson.save();
    return true;
  }

  @Mutation(() => Boolean)
  async assignSubjectsToLesson(
    @Arg('subjectIds', () => [ID]) subjectIds: string[],
    @Arg('lessonId', () => ID) lessonId: string
  ): Promise<boolean> {
    const subjects = await Subject.find({
      where: subjectIds.map((subjectId: string) => ({ id: subjectId }))
    });
    const lesson = await Lesson.findOneOrFail({ where: [{ id: lessonId }] });
    if (subjects.length == 0) return false;
    (await lesson.subjects).push(...subjects);
    await lesson.save();
    return true;
  }
}
