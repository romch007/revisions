import jwt from 'jsonwebtoken-promisified';

export type SignerType = 'STUDENT' | 'TEACHER' | 'ADMIN';

export class JWTService {
  public readonly expiresIn: string = '1 years';
  public readonly issuer: string = 'revisions-api';
  public readonly audience: string = 'revisions-frontend';
  public readonly subject: string = 'auth';

  private secret: string = process.env.JWT_SECRET ?? 'secret';

  async signToken(userId: string, signerType: SignerType): Promise<string> {
    const result = await jwt.signAsync({ userId, signerType }, this.secret, {
      expiresIn: this.expiresIn,
      issuer: this.issuer,
      audience: this.audience,
      subject: this.subject
    });
    return result;
  }

  async verifyToken(token: string): Promise<unknown> {
    const result = await jwt.verifyAsync(token, this.secret, {
      issuer: this.issuer,
      audience: this.audience,
      subject: this.subject
    });
    return result;
  }
}
