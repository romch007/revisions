import { Context, UserInfo } from './context';
import { AuthChecker } from 'type-graphql';
import { JWTService } from './services';

export const authChecker: AuthChecker<Context> = async ({ context }, roles) => {
  // Return false if no roles is given
  if (roles.length == 0) {
    return false;
  }

  const jwtService = new JWTService();

  const tokenRegex = /^Bearer (.+)$/;
  const match = tokenRegex.exec(String(context.req.headers['authorization']));

  if (!match) return false;

  let result: unknown;
  try {
    result = await jwtService.verifyToken(match[1]);
  } catch (err) {
    return false;
  }

  // Pass user data to resolver
  context.req.userInfo = result as UserInfo;

  return roles.includes(context.req.userInfo.signerType);
};
