// eslint-disable @typescript-eslint/ban-types
// @ts-ignore
export type Lazy<T extends object> = Promise<T> | T;
