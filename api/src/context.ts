import { Request, Response } from 'express';
import { SignerType } from './services';

export interface UserInfo {
  userId: string;
  signerType: SignerType;
}

export interface CustomRequest extends Request {
  userInfo: UserInfo;
}

export interface Context {
  req: CustomRequest;
  res: Response;
}
