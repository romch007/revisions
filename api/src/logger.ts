import { createLogger, format, transports } from 'winston';
const { combine, timestamp, printf } = format;

interface Input {
  level: string;
  message: string;
  timestamp?: Date;
}

const myFormat = printf((info: Input) => {
  return `${info.timestamp} [${info.level}] ${info.message}`;
});

const getLevel = () => {
  switch (process.env.NODE_ENV) {
    case 'production':
      return 'info';
    default:
      return 'debug';
  }
};

const logger = createLogger({
  level: getLevel(),
  format: combine(timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }), myFormat),
  transports: [new transports.Console()]
});

if (process.env.NODE_ENV === 'test') {
  logger.silent = true;
}

export default logger;
