import 'reflect-metadata';
import { createConnection } from 'typeorm';
import { ApolloServer } from 'apollo-server-express';
import express from 'express';
import { buildSchema } from 'type-graphql';
import http from 'http';

import logger from './logger';

const handleError = (err: Error): void => {
  logger.error(`Uncaught error: ${err.stack}`);
  return process.exit(1);
};

process
  .on('uncaughtException', handleError)
  .on('unhandledRejection', handleError);

import { authChecker } from './auth';

async function main() {
  logger.info('Starting API...');
  const app = express();

  await createConnection({
    type: 'postgres',
    host: process.env.DB_HOST ?? 'localhost',
    port: Number(process.env.DB_PORT ?? 5432),
    username: process.env.DB_USER ?? 'romain',
    password: process.env.DB_PASSWORD ?? 'test',
    database: process.env.DB_NAME ?? 'revisions',
    entities: [__dirname + '/models/**/*.{js,ts}'],
    synchronize: process.env.NODE_ENV !== 'production'
  }).catch(err => {
    logger.error(`Cannot connect to database: ${err}`);
    process.exit(1);
  });
  logger.info('Connection to database created');

  logger.debug('Creating schemas...');
  const schema = await buildSchema({
    resolvers: [__dirname + '/resolvers/**/*.{ts,js}'],
    authChecker,
    validate: false
  });
  logger.debug('Schemas created');

  const server = new ApolloServer({
    schema,
    context: ({ req, res }) => ({ req, res })
  });

  server.applyMiddleware({ app });

  const httpServer = http.createServer(app);
  server.installSubscriptionHandlers(httpServer);

  const port = process.env.PORT || 4000;

  httpServer.listen(port, () =>
    logger.info(`Server has started on port ${port}`)
  );
}

main();
