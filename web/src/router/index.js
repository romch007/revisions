import Vue from "vue";
import VueRouter from "vue-router";

import admin from "./admin";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "login",
    component: () => import(/* webpackChunkName: "login" */ "@/views/Login")
  },
  admin,
  {
    path: "*",
    name: "notfound",
    component: () =>
      import(/* webpackChunkName: "notfound" */ "@/views/NotFound")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
