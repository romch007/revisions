export default {
  path: "/admin",
  name: "admin-dashboard",
  components: {
    default: () =>
      import(/* webpackChunkName: "admin" */ "@/views/admin/AdminDashboard"),
    toolbar: () =>
      import(/* webpackChunkName: "admin" */ "@/views/admin/AdminToolbar")
  },
  children: [
    {
      path: "students",
      name: "students",
      component: () =>
        import(
          /* webpackChunkName: "admin" */ "@/views/admin/students/AdminStudent"
        )
    },
    {
      path: "students/:id",
      name: "student",
      component: () =>
        import(
          /* webpackChunkName: "admin" */ "@/views/admin/students/AdminStudentViewer"
        )
    },
    {
      path: "groups",
      name: "groups"
    },
    { path: "groups/:id", name: "group" },
    {
      path: "teachers",
      name: "teachers"
    },
    { path: "teachers/:id", name: "teacher" },
    {
      path: "admins",
      name: "admins"
    },
    { path: "admins/:id", name: "admin" },
    {
      path: "lessons",
      name: "lessons"
    },
    { path: "lessons/:id", name: "lesson" },
    {
      path: "subjects",
      name: "subjects"
    },
    { path: "subjects/:id", name: "subject" },
    {
      path: "classrooms",
      name: "classrooms"
    },
    { path: "classrooms/:id", name: "classroom" }
  ]
};
