import gql from "graphql-tag";

export const FETCH_STUDENTS = gql`
  query FetchStudents {
    students {
      id
      displayName
      user {
        username
        email
      }
      groups {
        id
        title
      }
    }
  }
`;

export const FETCH_STUDENT_INFOS = gql`
  query GetStudentInfo($studentId: ID!) {
    student(id: $studentId) {
      firstname
      lastname
      absent
      user {
        email
        username
      }
      groups {
        id
        title
        description
        students {
          id
        }
      }
      lessons {
        id
        title
        description
        startDate
        endDate
        teachers {
          id
          displayName
        }
        classroom {
          id
          title
        }
        groups {
          id
          title
        }
        subjects {
          id
          title
        }
      }
    }
  }
`;

export const MODIFY_STUDENT_INFOS = gql`
  mutation ModifyStudentInfos(
    $studentId: ID!
    $student: StudentModificationInput!
  ) {
    modifyStudent(studentId: $studentId, student: $student) {
      id
    }
  }
`;

export const CREATE_STUDENT = gql`
  mutation CreateStudent($student: StudentCreationInput!) {
    createStudent(student: $student) {
      id
    }
  }
`;

export const DELETE_STUDENT = gql`
  mutation DeleteStudent($studentId: ID!) {
    deleteStudent(studentId: $studentId)
  }
`;

export const ASSIGN_STUDENT_TO_GROUP = gql`
  mutation AssignStudentToGroup($studentId: ID!, $groupId: ID!) {
    assignStudentsToGroup(studentIds: [$studentId], groupId: $groupId)
  }
`;

export const ASSIGN_STUDENT_TO_LESSON = gql`
  mutation AssignStudentToLesson($studentId: ID!, $lessonId: ID!) {
    assignStudentsToLesson(studentIds: [$studentId], lessonId: $lessonId)
  }
`;
