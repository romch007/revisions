import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import vuetify from "./plugins/vuetify";
import { createProvider } from "./plugins/vue-apollo";
import i18n from "./plugins/i18n";

Vue.config.productionTip = false;

new Vue({
  router,
  vuetify,
  apolloProvider: createProvider(),
  i18n,
  render: h => h(App)
}).$mount("#app");
